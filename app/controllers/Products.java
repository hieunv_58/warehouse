package controllers;

import com.avaje.ebean.Page;
import com.google.common.io.Files;
import models.*;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.products.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import play.data.Form;
import play.mvc.Security;

import javax.imageio.ImageIO;

import static play.mvc.Http.MultipartFormData;

/**
 * Created by hieuUET2395 on 24/01/2015.
 */
@Security.Authenticated(Secured.class)
public class Products extends Controller {
    private static final Form<Product> productForm = Form.form(Product.class);

    public static Result list(Integer page) {
        Page<Product> products = Product.find(page);
        return ok(views.html.catalog.render(products));
    }

    public static Result newProduct() {
        return ok(details.render(productForm));
    }

    public static Result details(String ean) {
        final Product product = Product.findByEan(ean);
        if (product == null) {
            return notFound( String.format("Product %s does not exist.", ean) );
        }
        Form<Product> filledForm = productForm.fill(product);
        return ok(details.render(filledForm));
    }

    public static Result save() {
        Form<Product> boundForm = productForm.bindFromRequest();

        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(boundForm));
        }
        Product product = boundForm.get();
		MultipartFormData body = request().body().asMultipartFormData();
		MultipartFormData.FilePart part = body.getFile("picture");
		
		if(part != null) {
			File picture = part.getFile();
                try {
                    if(ImageIO.read(picture) == null){
                        flash("error", String.format("Uploaded file is not image file"));
                        return redirect(routes.Products.details(product.ean));
                    }
                        product.picture = Files.toByteArray(picture);
                } catch (IOException e){
                    return internalServerError("Error reading file upload");
                }

		}
		
        List<Tag> tags = new ArrayList<Tag>();
        for (Tag tag : product.tags) {
            if (tag.id != null) {
                tags.add(Tag.findById(tag.id));
            }
        }
        product.tags = tags;

        if(product.id == null) {
            if(Product.findByEan(product.ean) == null) {
                product.save();
            }
        }
        else {
            product.update();
        }

        StockItem stockItem = new StockItem();
        stockItem.product = product;
        stockItem.quantity = 0L;
        stockItem.save();

        flash("success", String.format("Successfully added product %s", product));
        return redirect(routes.Products.list(0));
    }

    public static Result delete(String ean) {
        final Product product = Product.findByEan(ean);
        if(product == null) {
            return notFound( String.format("Product %s does not exist.", ean) );
        }

        product.delete();
        return redirect(routes.Products.list(0));
    }
	
	public static Result picture(String ean) {
		final Product product = Product.findByEan(ean);
		if(product == null) {
            return notFound( String.format("Product %s does not exist", ean) );
        }
		return ok(product.picture);
	}
}
