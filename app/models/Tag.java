package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.*;
import java.util.*;
/**
 * Created by hieuUET2395 on 04/02/2015.
 */
@Entity
public class Tag extends Model  {
    @Id
    public Long id;

    @Constraints.Required
    public String name;

    @ManyToMany(mappedBy="tags")
    public List<Product> products;

    public static Finder<Long, Tag> find = new Finder<>(Long.class, Tag.class);
    public static Tag findById(Long id) {
        return find.byId(id);
    }



}
