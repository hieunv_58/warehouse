package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.*;
/**
 * Created by hieuUET2395 on 08/02/2015.
 */
@Entity
public class Warehouse extends Model {

    @Id
    public Long id;

    public String name;

    @OneToOne
    public Address address;

    @OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList();

    public static Warehouse findById(Long id) {
        return Warehouse.find.byId(id);
    }

    public static Finder<Long, Warehouse> find = new Finder<>(Long.class, Warehouse.class);

    public String toString() {
        return name;
    }
}
